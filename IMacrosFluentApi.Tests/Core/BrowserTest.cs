﻿using System;
using NUnit.Framework;

namespace IMacrosFluentApi.Tests.Core
{
	[TestFixture]
	public class BrowserTest
	{
		[TestCase("[EXTRACT]", Result = "")]
		[TestCase("#EANF#", Result = "")]
		[TestCase("[EXTRACT]#EANF#", Result = "")]
		[TestCase("1[EXTRACT]2", Result = "1")]
		public string ExtractTest(string source)
		{
			return source.Replace("#EANF#", "").Split(new[] { "[EXTRACT]" }, StringSplitOptions.None)[0];
		} 
	}
}