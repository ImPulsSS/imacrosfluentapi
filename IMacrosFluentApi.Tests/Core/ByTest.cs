﻿using IMacrosFluentApi.Core;
using IMacrosFluentApi.Core.Exceptions;
using NUnit.Framework;

namespace IMacrosFluentApi.Tests.Core
{
	[TestFixture]
	public class ByTest
	{
		[TestCase("div", Result = "POS=1 TYPE=DIV ATTR=*")]
		[TestCase("#test", Result = "POS=1 TYPE=* ATTR=ID:test")]
		[TestCase(".test", Result = "POS=1 TYPE=* ATTR=CLASS:*test*")]
		[TestCase(":text", Result = "POS=1 TYPE=*:TEXT ATTR=*")]
		[TestCase("::text", Result = "POS=1 TYPE=*:TEXT ATTR=*")]
		[TestCase("[name='test']", Result = "POS=1 TYPE=* ATTR=NAME:test")]
		[TestCase("[name^='test']", Result = "POS=1 TYPE=* ATTR=NAME:test*")]
		[TestCase("[name$='test']", Result = "POS=1 TYPE=* ATTR=NAME:*test")]
		[TestCase("[name*='test']", Result = "POS=1 TYPE=* ATTR=NAME:*test*")]
		[TestCase("input:text#test.test[name='test'][data='test2']", Result = "POS=1 TYPE=INPUT:TEXT ATTR=ID:test&&CLASS:*test*&&NAME:test&&DATA:test2")]

		[TestCase("a:b#c.d[e='f']", Result = "POS=1 TYPE=A:B ATTR=ID:c&&CLASS:*d*&&E:f")]

		[TestCase("#test > div", Result = "POS=1 TYPE=DIV FORM=ID:test ATTR=*")]
		[TestCase(".test > div", Result = "POS=1 TYPE=DIV FORM=CLASS:*test* ATTR=*")]
		[TestCase("[name='test'] > div", Result = "POS=1 TYPE=DIV FORM=NAME:test ATTR=*")]
		[TestCase("[name^='test'] > div", Result = "POS=1 TYPE=DIV FORM=NAME:test* ATTR=*")]
		[TestCase("[name$='test'] > div", Result = "POS=1 TYPE=DIV FORM=NAME:*test ATTR=*")]
		[TestCase("[name*='test'] > div", Result = "POS=1 TYPE=DIV FORM=NAME:*test* ATTR=*")]
		[TestCase("form#frm.frm[name='frm'] > input:text#test.test[name='test'][data='test2']", Result = "POS=1 TYPE=INPUT:TEXT FORM=ID:frm&&CLASS:*frm*&&NAME:frm ATTR=ID:test&&CLASS:*test*&&NAME:test&&DATA:test2")]
		public string SelectorTest(string selector)
		{
			return By.Selector(selector).ToString();
		}

        [TestCase("div > div", ExpectedException = typeof(MacrosException))] // parent must be form node type
        [TestCase("div > div > div", ExpectedException = typeof(TokenizeException))] // should be max two nested level
        public By UnsupportedSelectors(string selector)
	    {
            return By.Selector(selector);
	    }
	}
}
