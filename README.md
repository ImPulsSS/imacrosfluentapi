# iMacros Fluent Api#

Simple fluent interface for generating and executing [iMacros](http://imacros.net) browser macros.

### Install the package ###

You may need to install iMacros WebBrowser Component for .NET before (can be found [here](http://imacros.net/download)).

```
Install-Package IMacrosFluentApi –IncludePrerelease
```

### Sample usage ###

```
#!c#

using (var browser = new MacrosBrowser(new MacrosBrowser.Config()))
{
    browser
        .Goto("http://imacros.net/download")
        .OnDownload(Environment.CurrentDirectory, "*", true)
        .Find(By.Selector("a[type='button']").AndText("Download 64-bit")).Click(true).End()
        .Execute();
}
```