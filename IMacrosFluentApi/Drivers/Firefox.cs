﻿using System.Linq;
using IMacrosFluentApi.Core;
using IMacrosFluentApi.Core.Enums;
using IMacrosFluentApi.Core.Exceptions;

namespace IMacrosFluentApi.Drivers
{
	public class Firefox : Browser<Firefox>
	{
		#region CONSTRUCTORS

		public Firefox(Config config) : base(config)
		{

		}

		#endregion

		#region API

        #region SAVEAS

        public override Firefox SaveAs(SaveAsType type, string folder, string filename)
        {
            if (_unsupportedSaveAsTypes.Contains(type))
                throw new MacrosException("Unsupported SaveAs Type");

            return base.SaveAs(type, folder, filename);
        }

        #endregion

		#region SEARCH

		public override Firefox Search(SearchType type, string pattern, bool ignoreCase = false)
		{
			var tpl = "SEARCH SOURCE={0}:\"{1}\"";

			if (type == SearchType.REGEXP)
			{
				pattern = "(" + pattern + ")";

				tpl += " EXTRACT=\"$1\"";
			}

			if (ignoreCase)
				tpl += " IGNORE_CASE=YES";

			Queue(tpl, type, pattern);

			return this;
		}

		#endregion

		#endregion

		#region SaveAsType

	    private readonly SaveAsType[] _unsupportedSaveAsTypes =
	    {
	        SaveAsType.MHT, SaveAsType.BMP
	    };

		#endregion

		#region Config

		public new class Config : BrowserConfig
		{
			public string Profile;

			public override string GetCommnadLine()
			{
				return !string.IsNullOrEmpty(Profile) 
					? string.Format("-fx -fxProfile {0}", Profile) 
					: "-fx";
			}
		}

		#endregion
	}
}