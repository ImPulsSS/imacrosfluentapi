﻿using System.Linq;
using IMacrosFluentApi.Core;
using IMacrosFluentApi.Core.Enums;
using IMacrosFluentApi.Core.Exceptions;
using IMacrosFluentApi.Core.Interfaces;
using IMacrosFluentApi.Utils;

namespace IMacrosFluentApi.Drivers
{
	public class IE : Browser<IE>
	{
		#region CONSTRUCTORS

		public IE(Config config) : base(config)
		{
			
		}

		protected IE(IBrowserConfig config)
			: base(config)
		{
			
		}

		#endregion

		#region API

		#region SAVEAS

        public override IE SaveAs(SaveAsType type, string folder, string filename)
		{
            if (_unsupportedSaveAsTypes.Contains(type))
                throw new MacrosException("Unsupported SaveAs Type");

            return base.SaveAs(type, folder, filename);
		}

		#endregion

		#region SEARCH

		public override IE Search(SearchType type, string pattern, bool ignoreCase = false)
		{
			var tpl = "SEARCH SOURCE={0}:\"{1}\"";

			if (type == SearchType.REGEXP)
			{
				pattern = "(" + pattern.Replace("\\", "\\\\") + ")";
				 
				tpl += " EXTRACT=\"$1\"";
			}

			if (ignoreCase)
				tpl += " IGNORE_CASE=YES";

			Queue(tpl, type, pattern);

			return this;
		}

		#endregion

		#endregion

        #region SaveAsType

	    private readonly SaveAsType[] _unsupportedSaveAsTypes =
	    {
	        SaveAsType.JPEG
	    };

        #endregion

		#region Config

		public new class Config : BrowserConfig
		{
			public bool InPrivate;
			public IEVersion Version = IEVersion.lt10;

			public override void Bind(IBrowser browser)
			{
				base.Bind(browser);

				var firstExecution = true;

				Browser.BeforeExecute += (e, args) =>
					                         {
						                         if (!firstExecution)
							                         return;

												 if (ScreenSize != null)
												 {
													 args.CommandBuffer.PrependLineFormat("SIZE X={0} Y={1}",
													                                      ScreenSize.Width,
													                                      ScreenSize.Height);
												 }

						                         firstExecution = false;
					                         };
			}

			public override string GetCommnadLine()
			{
				return (Version == IEVersion.lt10
					        ? "-ie"
					        : "-ie_ext") + (InPrivate
						                        ? " -iePrivate"
						                        : "");
			}
		}

		#endregion
	}

	public enum IEVersion
	{
		lt10,
		gte10
	}
}