﻿using System;
using System.ComponentModel;
using System.Linq;
using IMacrosFluentApi.Core;
using IMacrosFluentApi.Core.Enums;
using IMacrosFluentApi.Core.Exceptions;

namespace IMacrosFluentApi.Drivers
{
	public class Chrome : Browser<Chrome>
	{
		#region CONSTRUCTORS

		public Chrome(Config config) : base(config)
		{

		}

		#endregion

		#region API

		#region SAVEAS

		public override Chrome SaveAs(SaveAsType type, string folder, string filename)
		{
            if (_unsupportedSaveAsTypes.Contains(type))
                throw new MacrosException("Unsupported SaveAs Type");

            return base.SaveAs(type, folder, filename);
		}

		#endregion

		#region ONDIALOG

		[EditorBrowsable(EditorBrowsableState.Never)]
		public override Chrome OnDialog(int pos, DialogButton button, string content = null)
		{
			throw new NotImplementedException();
		}

		#endregion

		#region SEARCH

		[EditorBrowsable(EditorBrowsableState.Never)]
		public override Chrome Search(SearchType type, string pattern, bool ignoreCase = false)
		{
			throw new NotImplementedException();
		}

		#endregion

		#endregion

		#region SaveAsType

	    private readonly SaveAsType[] _unsupportedSaveAsTypes =
	    {
	        SaveAsType.CPL,
	        SaveAsType.MHT,
	        SaveAsType.JPEG,
	        SaveAsType.BMP,
	        SaveAsType.JPEG
	    };
        
		#endregion

		#region Config

		public new class Config : BrowserConfig
		{
			public string UserDataDir;

			public override string GetCommnadLine()
			{
				return !string.IsNullOrEmpty(UserDataDir)
					? string.Format("-cr -crUserdataDir {0}", UserDataDir)
					: "-cr";
			}
		}

		#endregion
	}
}