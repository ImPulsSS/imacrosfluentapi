﻿using IMacrosFluentApi.Core;
using IMacrosFluentApi.Core.Interfaces;
using IMacrosFluentApi.Utils;

namespace IMacrosFluentApi.Drivers
{
	public class MacrosBrowser : IE
	{
		#region CONSTRUCTORS

		public MacrosBrowser(Config config) : base(config)
		{
			
		}

		#endregion

		#region Config

		public new class Config : BrowserConfig
		{
			public MacrosBrowserMode BrowserMode = MacrosBrowserMode.Normal;
            
			public override void Bind(IBrowser browser)
			{
				base.Bind(browser);

				var firstExecution = true;

				Browser.BeforeExecute += (e, args) =>
					                         {
						                         if (!firstExecution)
							                         return;

						                         if (ScreenSize != null)
						                         {
							                         args.CommandBuffer.PrependLineFormat("SIZE X={0} Y={1}",
							                                                              ScreenSize.Width,
							                                                              ScreenSize.Height);
						                         }

						                         firstExecution = false;
					                         };
			}

			public override string GetCommnadLine()
			{
				switch (BrowserMode)
				{
					case MacrosBrowserMode.Tray:
						return "-tray";

					case MacrosBrowserMode.Silent:
						return "-silent";

					case MacrosBrowserMode.Kiosk:
						return "-kioskmode";
				}

				return "";
			}
		}

		#endregion
	}

	public enum MacrosBrowserMode
	{
		Normal,
		Tray,
		Silent,
		Kiosk
	}
}