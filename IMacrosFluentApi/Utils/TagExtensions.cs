﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Reflection;
using IMacrosFluentApi.Core;

namespace IMacrosFluentApi.Utils
{
	public static class TagExtensions
	{
		public static Tag<T> Fill<T>(this Tag<T> form, NameValueCollection fields, bool doSubmit = true) where T : Browser<T>
		{
			Fill(form, fields.Cast<string>().ToDictionary(x => x, x => fields[x]), doSubmit);

			return form;
		}

		public static Tag<T> Fill<T>(this Tag<T> form, object fields, bool doSubmit = true) where T : Browser<T>
		{
			Fill(form,
				 fields.GetType()
					   .GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance)
					   .ToDictionary
						 (
							 propInfo => propInfo.Name,
							 propInfo => propInfo.GetValue(fields, null).ToString()
						 ),
				 doSubmit);

			return form;
		}

		public static Tag<T> Fill<T>(this Tag<T> form, IDictionary<string, string> fields, bool doSubmit = true) where T : Browser<T>
		{
			foreach (var field in fields)
			{
				form.Browser.Find(By.Form(form.Selector).AndName(field.Key)).SetValue(field.Value, true);
			}

			if (doSubmit)
			{
                form.Browser.Find(By.Form(form.Selector).AndAttr("type", "submit")).Click(true);
			}

			return form;
		}
	}
}