﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace IMacrosFluentApi.Utils
{
	public static class MacrosUtils
	{
		#region METHODS

		public static string Escape(string source)
		{
			return Escaper.Replace(source, m => ReplacementTable[m.Value]);
		}

		public static string EscapeJs(string source)
		{
			return Escape(source).Replace("\"", "\\\"");
		}

		public static string EscapeSp(string source)
		{
			return Escape(source).Replace(" ", "<SP>");
		}

		#endregion

		#region FIELDS

		private static readonly Regex Escaper = new Regex(@"\r|\n|\t", RegexOptions.Compiled);
		private static readonly IDictionary<string, string> ReplacementTable = new Dictionary<string, string>()
			                                                                 {
				                                                                 {"\r",	"\\r"},
				                                                                 {"\n",	"\\n"},
				                                                                 {"\t",	"\\t"}
			                                                                 };

		#endregion
	}
}