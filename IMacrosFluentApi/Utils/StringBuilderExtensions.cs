﻿using System;
using System.Globalization;
using System.Text;

namespace IMacrosFluentApi.Utils
{
	public static class StringBuilderExtensions
	{
		 public static StringBuilder AppendLineFormat(this StringBuilder builder, string template, params object[] args)
		 {
			 return AppendLineFormat(builder, new CultureInfo("en-US"), template, args);
		 }

		 public static StringBuilder AppendLineFormat(this StringBuilder builder, IFormatProvider formatProvider, string template, params object[] args)
		 {
			 return builder.AppendFormat(formatProvider, template + "\r\n", args);
		 }

		 public static StringBuilder PrependLineFormat(this StringBuilder builder, string template, params object[] args)
		 {
			 return PrependLineFormat(builder, new CultureInfo("en-US"), template, args);
		 }

		 public static StringBuilder PrependLineFormat(this StringBuilder builder, IFormatProvider formatProvider, string template, params object[] args)
		 {
			 return builder.Insert(0, string.Format(formatProvider, template + "\r\n", args));
		 }
	}
}