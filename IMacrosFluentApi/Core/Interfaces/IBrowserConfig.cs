﻿using IMacrosFluentApi.Core.Common;

namespace IMacrosFluentApi.Core.Interfaces
{
	public interface IBrowserConfig
	{
		int Timeout { get; }
		string UserAgent { get; }
		Proxy Proxy { get; }
		Size ScreenSize { get; }

		void Bind(IBrowser browser);
		string GetCommnadLine();
	}
}