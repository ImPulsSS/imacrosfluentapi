﻿using System;
using iMacros;
using IMacrosFluentApi.Core.Common;

namespace IMacrosFluentApi.Core.Interfaces
{
	public interface IBrowser
	{
		Status Execute();
		string Extract(int index);

		IBrowser Queue(string command, params object[] args);

		string GetErrorText();

		event EventHandler<BeforeExecuteEventArgs> BeforeExecute;
		event EventHandler<AfterExecuteEventArgs> AfterExecute;
	}
}