﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using IMacrosFluentApi.Core.Enums;
using IMacrosFluentApi.Utils;

namespace IMacrosFluentApi.Core
{
	public class Tag<T> where T : Browser<T>
	{
		#region CONSTRUCTORS

		private Tag(Browser<T> browser)
		{
			Browser = browser;
		}

		public Tag(Browser<T> browser, By selector) : this(browser)
		{
			Selector = selector;
		}

        public Tag(Browser<T> browser, string selector) : this(browser, By.Selector(selector))
        {
        }

		#endregion

		#region API

		public bool Exists
		{
			get
			{
                return !string.IsNullOrEmpty(Extract(ExtractType.HTM));
			}
		}

		public string Extract(ExtractType type)
		{
			Browser.Queue("TAG {0} EXTRACT={1}", Selector, type);

			return Browser.Extract(0);
		}

		public IEnumerable<Match> Extract(string pattern)
		{
            return Extract(new Regex(pattern, RegexOptions.IgnoreCase));
		}

		public IEnumerable<Match> Extract(Regex pattern)
		{
			var src = Browser.Queue("TAG {0} EXTRACT={1}", Selector, ExtractType.HTM).Extract(0);

			return !string.IsNullOrEmpty(src)
                ? pattern.Matches(src).Cast<Match>()
				: new Match[0];
		}

		public Tag<T> Click(bool force = false)
		{
			if (!force)
			{
				Browser.Queue("TAG {0}", Selector);
			}
			else 
			{
				Browser.Queue("TAG {0} EXTRACT=TXT", Selector);
				Browser.Queue("DS CMD=CLICK X={{{{!TAGX}}}} Y={{{{!TAGY}}}}");
				Browser.Queue("SET !EXTRACT NULL");
			}

			return this;
		}

		public Tag<T> MoveTo()
		{
			Browser.Queue("TAG {0} EXTRACT=TXT", Selector);
			Browser.Queue("DS CMD=MOVETO X={{{{!TAGX}}}} Y={{{{!TAGY}}}}");
			Browser.Queue("SET !EXTRACT NULL");

			return this;
		}

		public T End()
		{
			return (T)Browser;
		}

        #region FIND

        public Tag<T> Find(string selector)
        {
            return Find(By.Selector(selector));
        }

        public Tag<T> Find(By selector)
        {
            Browser.Queue("TAG {0} EXTRACT=TXT", Selector);
            Browser.Queue("SET !EXTRACT NULL");

            return new Tag<T>(Browser, selector.MakeRelative());
        }

        #endregion

        #region LEFT MOUSE BUTTON

        public Tag<T> LDown()
		{
			Browser.Queue("TAG {0} EXTRACT=TXT", Selector);
			Browser.Queue("DS CMD=LDOWN X={{{{!TAGX}}}} Y={{{{!TAGY}}}}");
			Browser.Queue("SET !EXTRACT NULL");

			return this;
		}

		public Tag<T> LUp()
		{
			Browser.Queue("TAG {0} EXTRACT=TXT", Selector);
			Browser.Queue("DS CMD=LUP X={{{{!TAGX}}}} Y={{{{!TAGY}}}}");
			Browser.Queue("SET !EXTRACT NULL");

			return this;
		}

		public Tag<T> LDblClicK()
		{
			Browser.Queue("TAG {0} EXTRACT=TXT", Selector);
			Browser.Queue("DS CMD=LDBLCLK X={{{{!TAGX}}}} Y={{{{!TAGY}}}}");
			Browser.Queue("SET !EXTRACT NULL");

			return this;
		}

		#endregion

		#region MIDDLE MOUSE BUTTON

		public Tag<T> MDown()
		{
			Browser.Queue("TAG {0} EXTRACT=TXT", Selector);
			Browser.Queue("DS CMD=MDOWN X={{{{!TAGX}}}} Y={{{{!TAGY}}}}");
			Browser.Queue("SET !EXTRACT NULL");

			return this;
		}

		public Tag<T> MUp()
		{
			Browser.Queue("TAG {0} EXTRACT=TXT", Selector);
			Browser.Queue("DS CMD=MUP X={{{{!TAGX}}}} Y={{{{!TAGY}}}}");
			Browser.Queue("SET !EXTRACT NULL");

			return this;
		}

		public Tag<T> MDblClick()
		{
			Browser.Queue("TAG {0} EXTRACT=TXT", Selector);
			Browser.Queue("DS CMD=MDBLCLK X={{{{!TAGX}}}} Y={{{{!TAGY}}}}");
			Browser.Queue("SET !EXTRACT NULL");

			return this;
		}

		#endregion

		#region RIGHT MOUSE BUTTON

		public Tag<T> RDown()
		{
			Browser.Queue("TAG {0} EXTRACT=TXT", Selector);
			Browser.Queue("DS CMD=RDOWN X={{{{!TAGX}}}} Y={{{{!TAGY}}}}");
			Browser.Queue("SET !EXTRACT NULL");

			return this;
		}

		public Tag<T> RUp()
		{
			Browser.Queue("TAG {0} EXTRACT=TXT", Selector);
			Browser.Queue("DS CMD=RUP X={{{{!TAGX}}}} Y={{{{!TAGY}}}}");
			Browser.Queue("SET !EXTRACT NULL");

			return this;
		}

		public Tag<T> RDblClick()
		{
			Browser.Queue("TAG {0} EXTRACT=TXT", Selector);
			Browser.Queue("DS CMD=RDBLCLK X={{{{!TAGX}}}} Y={{{{!TAGY}}}}");
			Browser.Queue("SET !EXTRACT NULL");

			return this;
		}

		#endregion

		#region CONTENT

        public Tag<T> Key(KeyPress value)
        {
            Browser.Queue("TAG {0} EXTRACT=TXT", Selector);
            Browser.Queue("DS CMD=KEY X={{{{!TAGX}}}} Y={{{{!TAGY}}}} CONTENT={{{0}}}", value);
            Browser.Queue("SET !EXTRACT NULL");

            return this;
        }

		public Tag<T> Key(string value)
		{
			Browser.Queue("TAG {0} EXTRACT=TXT", Selector);
            Browser.Queue("DS CMD=KEY X={{{{!TAGX}}}} Y={{{{!TAGY}}}} CONTENT=\"{0}\"", MacrosUtils.Escape(value));
			Browser.Queue("SET !EXTRACT NULL");

			return this;
		}

		public Tag<T> Content(string value, bool force = false)
		{
			if (!force)
			{
				Browser.Queue("TAG {0} CONTENT=\"{1}\"", Selector, MacrosUtils.Escape(value));
			}
			else
			{
				Browser.Queue("TAG {0} EXTRACT=TXT", Selector);
				Browser.Queue("DS CMD=CLICK X={{{{!TAGX}}}} Y={{{{!TAGY}}}} CONTENT=\"{0}\"", MacrosUtils.Escape(value));
				Browser.Queue("SET !EXTRACT NULL");
			}

			return this;
		}

		public Tag<T> SetValue(string value, bool force = false)
		{
			return Content(value, force);
		}

		#endregion

		#endregion

		#region FIELDS

		public readonly Browser<T> Browser;

		public readonly By Selector;

		#endregion
	}
}