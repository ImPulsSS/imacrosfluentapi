﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using iMacros;
using IMacrosFluentApi.Core.Common;
using IMacrosFluentApi.Core.Enums;
using IMacrosFluentApi.Core.Exceptions;
using IMacrosFluentApi.Core.Interfaces;
using IMacrosFluentApi.Utils;

namespace IMacrosFluentApi.Core
{
	[EditorBrowsable(EditorBrowsableState.Never)]
	public class Browser<T> : IDisposable, IBrowser where T : Browser<T>
	{
		#region CONSTRUCTORS

		protected Browser(IBrowserConfig config)
		{
			Config = config;
			Config.Bind(this);

			Player = new App();

			if (Player == null)
				throw new MacrosException("Unable to instantiate iMacros");

			if (Player.iimOpen(Config.GetCommnadLine(), true, Config.Timeout) != Status.sOk)
				throw new MacrosException("Unable to start browser");
		}

		public void Dispose()
		{
			Player.iimClose();
		}

		#endregion

		#region API

		#region URL

		public T Goto(string url)
		{
			Queue("URL GOTO={0}", url);

			return (T)this;
		}

		public T Goto(Uri url)
		{
			Queue("URL GOTO={0}", url);

			return (T)this;
		}

		public T Eval(string js)
		{
			Queue("URL GOTO=\"javascript:{0}\"", MacrosUtils.EscapeJs(js));

			return (T)this;
		}

		#endregion

		#region BACK

		public T Back()
		{
			Queue("BACK");

			return (T)this;
		}

		#endregion

		#region CLEAR

		public T Clear()
		{
			Queue("CLEAR");

			return (T)this;
		}

		#endregion

		#region FRAME

		public T SetFrame(int number)
		{
			Queue("FRAME F={0}", number);

			return (T)this;
		}

		public T SetFrame(string name)
		{
			Queue("FRAME NAME={0}", name);

			return (T)this;
		}

		#endregion

		#region ONDIALOG

		public virtual T OnDialog(int pos, DialogButton button, string content = null)
		{
			if (content != null) 
				Queue("ONDIALOG POS={0} BUTTON={1} CONTENT=\"{2}\"", pos, button, MacrosUtils.Escape(content));
			else
				Queue("ONDIALOG POS={0} BUTTON={1}", pos, button);

			return (T)this;
		}

		#endregion

		#region ONDOWNLOAD

		public virtual T OnDownload(string folder, string filename, bool waitForCompletion = false, string checksum = null)
		{
			var tpl = "ONDOWNLOAD FOLDER={0} FILE={1}";

			if (waitForCompletion)
				tpl += " WAIT=YES";

			if (!string.IsNullOrEmpty(checksum))
				tpl += " CHECKSUM={2}";


			Queue(tpl, folder, filename, checksum);

			return (T)this;
		}

		#endregion

		#region PAUSE

		[EditorBrowsable(EditorBrowsableState.Never)]
		public T Pause()
		{
			Queue("PAUSE");

			return (T)this;
		}

		#endregion

		#region PROXY

		[EditorBrowsable(EditorBrowsableState.Never)]
		public T Proxy(string address, string port = "8080")
		{
			Queue("PROXY ADDRESS={0}:{1}", address, port);

			return (T)this;
		}

		#endregion

		#region REFRESH

		public T Refresh()
		{
			Queue("REFRESH");

			return (T)this;
		}

		#endregion

		#region PROMPT

		public T Prompt(string text)
		{
			Queue("PROMPT {0}", MacrosUtils.EscapeSp(text));

			return (T)this;
		}

		public T Prompt(string variableName, string text)
		{
			Queue("PROMPT \"{0}\" {1}", MacrosUtils.Escape(text), variableName);

			return (T)this;
		}

		public T Prompt(string variableName, string text, object defaultValue)
		{
			Queue("PROMPT \"{0}\" {1} \"{2}\"", MacrosUtils.Escape(text), variableName, MacrosUtils.Escape(defaultValue.ToString()));

			return (T)this;
		}

		#endregion

        #region SAVEAS

        public virtual T SaveAs(SaveAsType type, string folder, string filename)
        {
            Queue("SAVEAS TYPE={0} FOLDER={1} FILE={2}", type, folder, filename);

            return (T)this;
        }

        #endregion

		#region SEARCH

		public virtual T Search(SearchType type, string pattern, bool ignoreCase = false)
		{
			var tpl = "SEARCH SOURCE={0}:{1}";

			if (ignoreCase)
				tpl += " IGNORE_CASE=YES";

			Queue(tpl, type, pattern);

			return (T)this;
		}

		public string GetSourceCode()
		{
			return ((T)this).Search(SearchType.REGEXP, @"[\s\S]*").Extract(0);
		}

		#endregion

		#region SET

		[EditorBrowsable(EditorBrowsableState.Never)]
		public T Set(string variable, string value)
		{
			Queue("SET !{0} {1}", variable, value);

			return (T)this;
		}
		
		#endregion
		
		#region TAB

		public T TabOpen()
		{
			Queue("TAB OPEN");

			return (T)this;
		}

		public T TabClose()
		{
			Queue("TAB CLOSE");

			return (T)this;
		}

		public T TabCloseAllOthers()
		{
			Queue("TAB CLOSEALLOTHERS");

			return (T)this;
		}

		public T SetTab(int number)
		{
			Queue("TAB T={0}", number);

			return (T)this;
		}

		#endregion

		#region TAG

		public Tag<T> Find(string selector)
		{
			return new Tag<T>(this, selector);
		}

		public Tag<T> Find(By selector)
		{
			return new Tag<T>(this, selector);
		}

        public IEnumerable<Tag<T>> FindAll(string selector)
        {
            return FindAll(By.Selector(selector));
        }

        public IEnumerable<Tag<T>> FindAll(By selector)
        {
            var index = 1;

            while (true)
            {
                var tag = new Tag<T>(this, selector.AndPos(index++));

                if (!tag.Exists)
                    break;

                yield return tag;
            }
        }

		#endregion

		#region VERSION

		[EditorBrowsable(EditorBrowsableState.Never)]
		public T Version(string versionNumber)
		{
			Queue("VERSION BUILD={0}", versionNumber);

			return (T)this;
		}

		#endregion

		#region WAIT

		public T Wait(double milliseconds)
		{
			Queue("WAIT SECONDS={0}", milliseconds / 1000);

			return (T)this;
		}

		#endregion
		
		#endregion

		#region METHODS

		public Status Execute()
		{
			var args = new BeforeExecuteEventArgs(CommandBuffer);

			if (BeforeExecute != null)
				BeforeExecute(this, args);

			var result = Player.iimPlayCode(CommandBuffer.ToString(), Config.Timeout);

			CommandBuffer.Clear();

			if (AfterExecute != null)
				AfterExecute(this, new AfterExecuteEventArgs(result));

			return result;
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public string Extract(int index)
		{
			return Execute() == Status.sOk
				? Player.iimGetExtract(index).Replace("#EANF#", "").Split(new[] { "[EXTRACT]" }, StringSplitOptions.None)[0]
				: null;
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public IBrowser Queue(string command, params object[] args)
		{
			if (args == null)
				CommandBuffer.AppendLine(command);
			else
				CommandBuffer.AppendLineFormat(command, args);

			return this;
		}

		public string GetErrorText()
		{
			return Player.iimGetErrorText();
		}
		
		#endregion

		#region FIELDS

		protected readonly App Player;
		protected readonly StringBuilder CommandBuffer = new StringBuilder();

		public readonly IBrowserConfig Config;

		public event EventHandler<BeforeExecuteEventArgs> BeforeExecute;
		public event EventHandler<AfterExecuteEventArgs> AfterExecute;

		#endregion
	}
}
