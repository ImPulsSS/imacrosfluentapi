﻿namespace IMacrosFluentApi.Core.Common
{
	public class Size
	{
		public int Width;
		public int Height;
	}
}