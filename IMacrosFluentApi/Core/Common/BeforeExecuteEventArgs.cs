﻿using System;
using System.Text;

namespace IMacrosFluentApi.Core.Common
{
	public class BeforeExecuteEventArgs : EventArgs
	{
		public BeforeExecuteEventArgs()
		{
		}

		public BeforeExecuteEventArgs(StringBuilder commandBuffer)
		{
			CommandBuffer = commandBuffer;
		}

		public StringBuilder CommandBuffer { get; set; }
	}
}