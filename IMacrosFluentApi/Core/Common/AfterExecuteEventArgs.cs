﻿using System;
using iMacros;

namespace IMacrosFluentApi.Core.Common
{
	public class AfterExecuteEventArgs : EventArgs
	{
		public AfterExecuteEventArgs()
		{
		}

		public AfterExecuteEventArgs(Status lastResult)
		{
			LastResult = lastResult;
		}

		public Status LastResult { get; set; }
	}
}