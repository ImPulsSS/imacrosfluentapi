﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Text.RegularExpressions;

namespace IMacrosFluentApi.Core.Common
{
	[TypeConverter(typeof(ProxyTypeConverter))]
	public class Proxy
	{
		public string Address { get; set; }
		public string Login { get; set; }
		public string Password { get; set; }
	}

	#region ProxyTypeConverter

	public class ProxyTypeConverter : TypeConverter
	{
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
		}

		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			return destinationType == typeof(string) || base.CanConvertTo(context, destinationType);
		}

		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			var strValue = (string) value;

			if (string.IsNullOrEmpty(strValue))
				return null;

			var matches = Regex.Match(strValue, Pattern);

			return new Proxy
				       {
						   Address = matches.Groups["Address"].Value,
						   Login = matches.Groups["Login"].Value,
						   Password = matches.Groups["Password"].Value
				       };
		}

		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			var proxy = (Proxy) value;

			var auth = proxy.Login + (!string.IsNullOrEmpty(proxy.Password)
				                               ? ":" + proxy.Password
				                               : "");

			return (!string.IsNullOrEmpty(auth) ? auth + "@" : "") + proxy.Address;
		}

		private const string Pattern = @"^(?:(?<Login>[^:@\n]+?)(?:\:(?<Password>[^@\n]+?))?@)?(?<Address>[\w\.\:]+?)$";
	}

	#endregion
}
