﻿using System.Collections.Generic;

namespace IMacrosFluentApi.Core.Enums
{
	public enum ByType
	{
		XPATH,
		POS,
		TYPE,
		FORM,
		ATTR
	}

	public class ByTypeComparer : IComparer<ByType>
	{
		public int Compare(ByType x, ByType y)
		{
			return x.GetHashCode() > y.GetHashCode()
				? 1
				: x.GetHashCode() < y.GetHashCode()
					? -1
					: 0;
		}
	}
}