﻿namespace IMacrosFluentApi.Core.Enums
{
	public enum SearchType
	{
		TXT,
		REGEXP
	}
}