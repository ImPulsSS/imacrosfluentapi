﻿namespace IMacrosFluentApi.Core.Enums
{
    public enum KeyPress
    {
        ENTER,
        BACKSPACE
    }
}