﻿namespace IMacrosFluentApi.Core.Enums
{
	public enum DialogButton
	{
		YES,
		NO,
		CANCEL
	}
}