﻿namespace IMacrosFluentApi.Core.Enums
{
	public enum ReplaySpeed
	{
		SLOW,
		MEDIUM,
		FAST
	}
}