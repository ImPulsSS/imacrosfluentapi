﻿namespace IMacrosFluentApi.Core.Enums
{
	public enum ExtractType
	{
		TXT,
		TXTALL,
		HTM,
		HREF,
		ALT,
		CHECKED
	}
}