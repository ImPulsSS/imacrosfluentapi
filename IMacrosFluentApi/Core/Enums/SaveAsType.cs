﻿namespace IMacrosFluentApi.Core.Enums
{
    public enum SaveAsType
    {
        CPL,
        MHT,
        HTM,
        TXT,
        EXTRACT,
        BMP,
        PNG,
        JPEG
    }
}