﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using IMacrosFluentApi.Core.Enums;
using IMacrosFluentApi.Core.Exceptions;
using IMacrosFluentApi.Utils;

namespace IMacrosFluentApi.Core
{
	public class By
	{
		#region CONSTRUCTORS

		protected By(ByType type, string query, params object[] args)
		{
			Add(type, query, args);
		}

		#endregion

		#region METHODS

		protected void Add(ByType type, string query, params object[] args)
		{
			if (!Query.ContainsKey(type))
				Query[type] = new List<string>();

			switch (type)
			{
				case ByType.ATTR:
				case ByType.FORM:
					Query[type].Add(string.Format(query, args));
					break;

				default:
					if (Query[type].Count == 0)
						Query[type].Add(string.Format(query, args));
					else
						Query[type][0] = string.Format(query, args);

					break;
			}
			
		}

		public By AndAttr(string name, string value)
		{
			Add(ByType.ATTR, "{0}:{1}", name.ToUpper(), value);

			return this;
		}

		public By AndClassName(string className)
		{
			return AndAttr("CLASS", "*" + className + "*");
		}

		public By AndId(string id)
		{
			return AndAttr("ID", id);
		}

		public By AndName(string name)
		{
			return AndAttr("NAME", name);
		}

		public By AndForm(string query)
		{
			Add(ByType.FORM, query);

			return this;
		}

        public By AndForm(IEnumerable<string> query)
		{
			Add(ByType.FORM, string.Join("&&", query));

			return this;
		}

        public By AndForm(By selector)
		{
			Add(ByType.FORM, string.Join("&&", selector.Query[ByType.ATTR]));

			return this;
		}

        public By AndPos(int index, bool relative = false)
		{
            Add(ByType.POS, (relative ? "R" : "") + index);

			return this;
		}

		public By AndTagName(string tagName)
		{
			Add(ByType.TYPE, tagName);

			return this;
		}

		public By AndXpath(string xpath)
		{
			Add(ByType.XPATH, xpath);

			return this;
		}

		public By AndText(string text)
		{
            return AndAttr("TXT", text);
		}

        public By MakeRelative()
        {
            if (Query.ContainsKey(ByType.POS) && Query[ByType.POS].Any())
            {
                Query[ByType.POS][0] = "R" + Query[ByType.POS][0].Trim('R');
            }
            else
            {
                AndPos(1, true);
            }

            return this;
        }

		public override string ToString()
		{
			if (!Query.ContainsKey(ByType.POS))
				Query[ByType.POS] = new List<string>();
			
			if (Query[ByType.POS].Count == 0)
				Query[ByType.POS].Add("1");

			if (!Query.ContainsKey(ByType.TYPE))
				Query[ByType.TYPE] = new List<string>();

			if (Query[ByType.TYPE].Count == 0)
				Query[ByType.TYPE].Add("*");

			if (!Query.ContainsKey(ByType.ATTR))
				Query[ByType.ATTR] = new List<string>();

			if (Query[ByType.ATTR].Count == 0)
				Query[ByType.ATTR].Add("*");

			return string.Join(" ", Query.OrderBy(x => x.Key, new ByTypeComparer()).Select(x =>
												 string.Format("{0}={1}",
															   x.Key,
															   string.Join("&&", x.Value.Select(MacrosUtils.EscapeSp)))));
		}

		#endregion

		#region STATIC METHODS

		public static By Attr(string name, string value)
		{
			return new By(ByType.ATTR, "{0}:{1}", name.ToUpper(), value);
		}

		public static By ClassName(string className)
		{
			return Attr("CLASS", "*" + className + "*");
		}

		public static By Id(string id)
		{
			return Attr("ID", id);
		}

		public static By Name(string name)
		{
			return Attr("NAME", name);
		}

		public static By Form(string query)
		{
			return new By(ByType.FORM, query);
		}

		public static By Form(IEnumerable<string> query)
		{
			return new By(ByType.FORM, string.Join("&&", query));
		}

		public static By Form(By selector)
		{
			return new By(ByType.FORM, string.Join("&&", selector.Query[ByType.ATTR]));
		}

		public static By Pos(int index, bool relative = false)
		{
			return new By(ByType.POS, (relative ? "R" : "") + index);
		}

		/// <summary>
		/// Parses subset of css selectors (only two nesting levels supported)
        /// Example: "form#formId.formClass1.formClass2[name='formName'] input:checkbox#id.class1.class2[attr1='test1'][attr2='test2']
		/// Order of selectors matters ([tag] > [pseudoclass] > [id] > [classes] > [attributes])
		/// Attribute selector matches: =, ^=, $=, *=
		/// </summary>
		public static By Selector(string selector)
		{
			var match = Tokenizer.Match(selector);

			if (!match.Success)
				throw new TokenizeException();

			var tag = match.Groups["type"].Success
						  ? match.Groups["type"].Value
						  : "*";

			if (match.Groups["pseudo"].Success)
				tag += ":" + match.Groups["pseudo"].Value;

			var result = TagName(tag);
		    var parent = GetParent(match);

		    if (parent != null)
		    {
		        var tagName = parent.GetTagName();
                if (!SupportedParentNodes.Contains(tagName))
                    throw new MacrosException("Only FORM parent node supported");

                result.AndForm(parent);
		    }

			if (match.Groups["id"].Success)
				result.AndId(match.Groups["id"].Value);

			if (match.Groups["class"].Success)
			{
				foreach (Capture capture in match.Groups["class"].Captures)
				{
					result.AndClassName(capture.Value);
				}
			}

			if (match.Groups["attrName"].Success)
			{
				for (var i = 0; i < match.Groups["attrName"].Captures.Count; i++)
				{
					var value = match.Groups["attrValue"].Captures[i].Value;

					switch (match.Groups["attrModifier"].Captures[i].Value)
					{
						case "^=":
							value = value + "*";
							break;

						case "$=":
							value = "*" + value;
							break;

						case "*=":
							value = "*" + value + "*";
							break;
					}

					result.AndAttr(match.Groups["attrName"].Captures[i].Value, value);
				}
			}

			return result;
		}

	    public static By TagName(string tagName)
		{
			return new By(ByType.TYPE, tagName.ToUpper());
		}

		public static By Xpath(string xpath)
		{
			return new By(ByType.XPATH, xpath);
		}

        private string GetTagName()
        {
            return Query.ContainsKey(ByType.TYPE) && Query[ByType.TYPE].Count > 0
                ? Query[ByType.TYPE].First().ToUpper()
                : "*";
        }

	    private static By GetParent(Match match)
	    {
	        var success = false;
            var parent = By.Pos(1);

	        if (match.Groups["parentType"].Success)
	        {
	            success = true;
                parent.AndTagName(match.Groups["parentType"].Value);
	        }

	        if (match.Groups["parentId"].Success)
	        {
                success = true;
                parent.AndId(match.Groups["parentId"].Value);
	        }

	        if (match.Groups["parentClass"].Success)
	        {
                success = true;
                parent.AndAttr("CLASS", "*" + match.Groups["parentClass"].Value + "*");
	        }

            if (match.Groups["parentAttrName"].Success)
            {
                success = true;

                for (var i = 0; i < match.Groups["parentAttrName"].Captures.Count; i++)
                {
                    var value = match.Groups["parentAttrValue"].Captures[i].Value;

                    switch (match.Groups["parentAttrModifier"].Captures[i].Value)
                    {
                        case "^=":
                            value = value + "*";
                            break;

                        case "$=":
                            value = "*" + value;
                            break;

                        case "*=":
                            value = "*" + value + "*";
                            break;
                    }

                    parent.AndAttr(match.Groups["parentAttrName"].Captures[i].Value, value);
                }
            }

	        if (!success)
	            return null;

	        return parent;
	    }

		#endregion

		#region FIELDS

		public Dictionary<ByType, List<string>> Query = new Dictionary<ByType, List<string>>();

	    private static readonly string[] SupportedParentNodes = { "FORM", "*" };

		private static readonly Regex Tokenizer =
			new Regex(@"^(?:(?<parentType>[_a-z][_a-z0-9-]*|\*)?(?:#(?<parentId>[_a-z][_a-z0-9-]*))?(?:\.(?<parentClass>[_a-z][_a-z0-9-]*))*(?:\[(?<parentAttrName>[_a-z][_a-z0-9-]*)\s*(?<parentAttrModifier>[\^\$\*]?=)\s*['""](?<parentAttrValue>[^'""]+)['""]\])*(?:\s+|\s*>\s*)|)(?<type>[_a-z][_a-z0-9-]*|\*)?(?:::?(?<pseudo>[_a-z][_a-z0-9-]*))?(?:#(?<id>[_a-z][_a-z0-9-]*))?(?:\.(?<class>[_a-z][_a-z0-9-]*))*(?:\[(?<attrName>[_a-z][_a-z0-9-]*)\s*(?<attrModifier>[\^\$\*]?=)\s*['""](?<attrValue>[^'""]+)['""]\])*$",
				RegexOptions.Compiled | RegexOptions.IgnoreCase);

		#endregion
	}
}