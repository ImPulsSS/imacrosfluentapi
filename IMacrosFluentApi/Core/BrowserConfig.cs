﻿using IMacrosFluentApi.Core.Common;
using IMacrosFluentApi.Core.Enums;
using IMacrosFluentApi.Core.Interfaces;
using IMacrosFluentApi.Utils;

namespace IMacrosFluentApi.Core
{
	public class BrowserConfig : IBrowserConfig
	{
		#region CONSTRUCTORS

		public BrowserConfig()
		{
			Timeout = 30;
		}

		#endregion

		#region METHODS

		public virtual void Bind(IBrowser browser)
		{
			Browser = browser;

			var firstExecution = true;

			Browser.BeforeExecute += (e, args) =>
				                         {
											 if (!string.IsNullOrEmpty(UserAgent))
											 {
												 args.CommandBuffer.PrependLineFormat("SET !USERAGENT \"{0}\"", UserAgent);
											 }

											 if (ReplaySpeed != null)
											 {
												 args.CommandBuffer.PrependLineFormat("SET !REPLAYSPEED {0}", ReplaySpeed);
											 }

											 // **************************

											 if (!firstExecution)
												 return;

											 if (Proxy != null && !string.IsNullOrEmpty(Proxy.Address))
											 {
												 if (!string.IsNullOrEmpty(Proxy.Login))
												 {
													 args.CommandBuffer.PrependLineFormat("ONLOGIN USER={0} PASSWORD={1}", Proxy.Login, Proxy.Password);
												 }

												 args.CommandBuffer.PrependLineFormat("PROXY ADDRESS={0}", Proxy.Address);
											 }

											 firstExecution = false;
				                         };
		}

		public virtual string GetCommnadLine()
		{
			return "";
		}

		#endregion

		#region PROPERTIES

		public int Timeout { get; set; }
		public string UserAgent { get; set; }
		public ReplaySpeed? ReplaySpeed { get; set; }
		public Proxy Proxy { get; set; }
		public Size ScreenSize { get; set; }

		protected IBrowser Browser;

		#endregion
	}
}