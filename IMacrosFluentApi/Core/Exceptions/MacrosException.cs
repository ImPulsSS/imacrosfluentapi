﻿using System;

namespace IMacrosFluentApi.Core.Exceptions
{
	public class MacrosException : Exception
	{
		public MacrosException(string message)
			: base(message)
		{
		}

		public MacrosException(string message, params object[] formatParams)
			: base(string.Format(message, formatParams))
		{
		}
	}
}
