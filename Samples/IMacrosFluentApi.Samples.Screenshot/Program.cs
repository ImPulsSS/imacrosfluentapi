﻿using System;
using IMacrosFluentApi.Core.Common;
using IMacrosFluentApi.Core.Enums;
using IMacrosFluentApi.Core.Interfaces;
using IMacrosFluentApi.Drivers;

namespace IMacrosFluentApi.Samples.Screenshot
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("Usage: screenshot <twitter-account>");
                Console.ReadKey();
                return;
            }

            var twitterAccount = args[0];

            using (var browser = new MacrosBrowser((MacrosBrowser.Config) GetConfig()))
            {
                browser
                    .Goto("https://twitter.com/" + twitterAccount)
                    .SaveAs(SaveAsType.PNG, AppDomain.CurrentDomain.BaseDirectory, twitterAccount + ".png")
                    .Execute();
            }
        }

        static IBrowserConfig GetConfig()
        {
            return new MacrosBrowser.Config
            {
                ScreenSize = new Size
                {
                    Width = 1024,
                    Height = 768
                }
            };
        }
    }
}
