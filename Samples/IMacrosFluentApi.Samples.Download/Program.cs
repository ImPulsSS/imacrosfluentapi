﻿using System;
using iMacros;
using IMacrosFluentApi.Core;
using IMacrosFluentApi.Core.Interfaces;
using IMacrosFluentApi.Drivers;

namespace IMacrosFluentApi.Samples.Download
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var browser = new MacrosBrowser(new MacrosBrowser.Config()))
            {
                browser.BeforeExecute += (sender, e) => Console.WriteLine(e.CommandBuffer.ToString());
                browser.AfterExecute += (sender, e) => Console.WriteLine(e.LastResult != Status.sOk
                    ? "error: " + ((IBrowser) sender).GetErrorText()
                    : "completed with no errors");

                browser
                    .Goto("http://imacros.net/download")
                    .OnDownload(AppDomain.CurrentDomain.BaseDirectory, "*", true)
                    .Find(By.Selector("a[type='button']").AndText("Download 64-bit")).Click(true).End()
                    .Execute();
            }

            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }
    }
}
