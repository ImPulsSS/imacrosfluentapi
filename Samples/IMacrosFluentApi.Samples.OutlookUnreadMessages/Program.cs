﻿using System;
using System.Collections.Specialized;
using IMacrosFluentApi.Core.Enums;
using IMacrosFluentApi.Drivers;
using IMacrosFluentApi.Utils;

namespace IMacrosFluentApi.Samples.OutlookUnreadMessages
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Usage: OutlookUnreadMessages <email> <password>");
                Console.ReadKey();
                return;
            }

            var email = args[0];
            var password = args[1];

            using (var browser = new MacrosBrowser(new MacrosBrowser.Config()))
            {
                browser.Goto("http://www.outlook.com");

                // login
                browser.Find("form[name='f1']")
                       .Fill(new NameValueCollection
                             {
                                 {"login", email},
                                 {"passwd", password}
                             })
                       .End();

                // check if we in inbox tab
                if (!browser.Find("li.ItemSelected[nm='Inbox']").Exists)
                    browser.Find("li[nm='Inbox']").Click(true);

                // get unread messages
                var messages = browser.FindAll("li.mlUnrd");
                foreach (var message in messages)
                {
                    // output message subject
                    Console.WriteLine(message.Find("a.t_elnk").Extract(ExtractType.TXT));
                }
                    
                // logout
                browser.Find("#c_melink").Click(true).End()
                       .Find("#c_signout").MoveTo().Click(true).End();

                browser.Execute();
            }
        }
    }
}
